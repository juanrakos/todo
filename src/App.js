import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Nav from "./nav.js"
import Form from "./todoform.js"

class App extends Component {

  constructor(){
    super();
    this.state = { 
      todos: []   
     }
  this.addTodo = this.addTodo.bind(this);
  }

   addTodo(todo){
     console.log(todo)
    this.setState(
      {
        todos: [...this.state.todos,todo]

      }

    )

  }

  render() {
  const todos =  this.state.todos.map((todo,i)=> {
    return (  
      <div key={i} className="card col-4">
        {todo}
        </div>
    )
    }
    
    
    )
    console.log(this.state.todos.length)
    return (
      <div className="App">
        <Nav count={this.state.todos.length}/>
        <header className="App-header">
          <Form todo={this.addTodo}  />
          </header>
          <div className="row">
          
        {todos}
      
        </div>
      </div>
    );
  }
}

export default App;
