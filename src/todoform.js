import React, { Component } from 'react';

class Form extends Component{
constructor(props){
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
}
onSubmit(e){
    e.preventDefault();
    const reutilizar = e.target.querySelector('.task-name');
    this.props.todo(reutilizar.value);
    reutilizar.value = "";
   // console.log(e.target.querySelector('.task-name').value)
}

render(){
    return (
        <div>
        <form className="form-group" onSubmit={(e) => this.onSubmit(e)}>
        <div>
        <input className="task-name" type="text" name="name" placeholder="Nombre de la tarea">
        </input>
        </div>
        <div>
            <button className="btn btn-medium btn-info" type="submit">
                Agregar Tarea
            </button>
        </div>
        </form>
        </div>
    )
}

}

export default Form;